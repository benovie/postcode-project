<?php
require '../vendor/autoload.php';

/**
 * setup app
 */
$appSettings = array(
	'debug' => false,
	'mysql.host' => 'lnx0101',
	'mysql.db' => 'postcode',
	'mysql.user' => 'postcode',
	'mysql.password' => '67hcaH2'
);
$app = new \Slim\Slim($appSettings);
$appErrorHandler = function($message, $code) use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');
	$error = new \stdClass();
	$error->message = $message;
	$error->code = $code;
	echo json_encode($error);
};
$app->db = function() use ($app) {
	static $instance;
	if (!isset($instance)) {
		$instance = new \Simplon\Mysql\Mysql($app->config('mysql.host'),$app->config('mysql.user'),$app->config('mysql.password'),$app->config('mysql.db'));
	}
	return $instance;
};
/**
 * app error handling
 */
$app->error(function ($exception) use ($app, $appErrorHandler) {
	$error = new \stdClass();
	switch (get_class($exception)) {
		case "Simplon\Mysql\MysqlException":
			$jsonMessage = json_decode($exception->getMessage());
			$appErrorHandler($jsonMessage ? $jsonMessage->errorInfo->message : $exception->getMessage(), $jsonMessage ? $jsonMessage->errorInfo->sqlStateCode : $exception->getCode());
		break;
		default:
			$appErrorHandler($exception->getMessage(), $exception->getCode());
	}
});
$app->notFound(function () use ($app, $appErrorHandler) {
	$appErrorHandler('Not found', 404);
});
/**
 * postcode route
 */
$app->get('/:postcode/:huisnummer', function ($postcode, $huisnummer) use ($app) {
	$postcode = strtoupper(str_replace(' ', '', $postcode));
	$huisnummer = (int)$huisnummer;
	$sql = 'SELECT 	`straatnaam`, `plaatsnaam`, `gemeente`
			FROM 	`adres`
			WHERE 	`postcode` = :postcode
				AND `van` <= :nr
				AND `tot` >= :nr';
	$result = $app->db->fetchRow($sql, array('postcode' => $postcode, 'nr' => $huisnummer));
	if (!$result) {
		$app->notFound();
	}
	$app->response->headers->set('Content-Type', 'application/json');
	$app->response->body(json_encode($result));
});

$app->run();